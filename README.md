# Dockerized Interactive Brokers API service

> This is an up-to-date fork from [Pierre Penninckx's original repository](https://github.com/ibizaman/docker-ibcontroller).

## Usage

The IB Gateway API will always be available on port `4003`.

Customize the service by providing any [IBC setting](https://github.com/IbcAlpha/IBC/blob/master/resources/config.ini) as an environment variable prefixed with `IB_`.

```yml
services:
  ib:
    image: registry.gitlab.com/iamadriaperez/docker-ibcontroller:latest
    environment:
      IB_TradingMode: paper
      IB_IbLoginId: username
      IB_IbPassword: password
```

### Read-only access

Without further configuration, the IB Gateway API will only be available in read-only mode. A message like this one will appear in the logs if you try to, say, place an order:

```text
IBC: Detected dialog entitled: API client needs write access action confirmation
```

To solve this, you have to provide a customized IB Gateway XML settings file **for each account** that you'll be using. First, log in to every account in your local machine using the official IB Gateway app and customize all the settings that you want. Remember to uncheck `Configuration > API > Settings > Read-Only API`. Exit the application and make the XML file/s accessible inside the container:

```yml
services:
  ib:
    image: registry.gitlab.com/iamadriaperez/docker-ibcontroller:latest
    environment:
      IB_TradingMode: paper
      IB_IbLoginId: username
      IB_IbPassword: password
    volumes:
      - ~/.ib-tws/youraccounthash/ibg.xml:/var/run/ibc/tws/conf/youraccounthash/ibg.xml
```

**Warning: Note that your personal account information will now be available inside the docker container!**

## Releases

### `latest`

Rebuilt automatically every Monday. Additionally, after each commit (and merge) to master.

### `release-{version}-{year}-{week}`

Snapshot of the `latest` tag after the automated weekly build on Mondays.

## Disclaimer

None of the authors, contributors, administrators, or anyone else connected with this repository, in any way whatsoever, can be responsible for your use of the code/software contained in or linked from this repository.

When using any of the provided code/software in this repository you accept any responsibility or liability of any use made by you, whether or not arising from the negligence of any of this repository's authors, contributors, or administrators.
